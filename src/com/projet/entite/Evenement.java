/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entite;

import java.sql.Date;

/**
 *
 * @author zied
 */
public class Evenement {
    private int id_evenement,id_club;
    private String description,lieu,image;
    private Date date_event,date_partage;
    private float duree;

    public Evenement() {
    }

    public Evenement(int id_evenement, int id_club, String description, String lieu, String image, Date date_event,  float duree) {
        this.id_evenement = id_evenement;
        this.id_club = id_club;
        this.description = description;
        this.lieu = lieu;
        this.image = image;
        this.date_event = date_event;
        //this.date_partage = date_partage;
        this.duree = duree;
    }

    public int getId_evenement() {
        return id_evenement;
    }

    public void setId_evenement(int id_evenement) {
        this.id_evenement = id_evenement;
    }

    public long getId_club() {
        return id_club;
    }

    public void setId_club(int id_club) {
        this.id_club = id_club;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDate_event() {
        return date_event;
    }

    public void setDate_event(Date date_event) {
        this.date_event = date_event;
    }

    public Date getDate_partage() {
        return date_partage;
    }

    public void setDate_partage(Date date_partage) {
        this.date_partage = date_partage;
    }

    public float getDuree() {
        return duree;
    }

    public void setDuree(float duree) {
        this.duree = duree;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id_evenement=" + id_evenement + ", id_club=" + id_club + ", description=" + description + ", lieu=" + lieu + ", image=" + image + ", date_event=" + date_event + ", date_partage=" + date_partage + ", duree=" + duree + '}';
    }
    
    
}
