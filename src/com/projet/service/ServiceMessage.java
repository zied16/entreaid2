/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.service;

import com.esprit.utils.DataSource;
import com.projet.entite.InscriptionGroupProjet;
import com.projet.entite.Message;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author zied
 */
public class ServiceMessage {
    
      private Connection con = DataSource.getInstance().getConnection();
    private Statement ste;

    public ServiceMessage() {
        try {
            ste = con.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    public void CreerMessage(Message M) throws SQLException
    {
        String requete="SELECT * FROM `users` WHERE id_users=" + M.getId_user() + "";
        ResultSet res= ste.executeQuery(requete);
        
        if(res.first())
        {
            String req = "INSERT INTO `message`(`id_message`, `contenu`, `date_partage`, `document`, `destination`, `id_users`)" 
                    + " VALUES (NULL,'" + M.getContenu()+"' ,SYSDATE(),'" + M.getDocument()+"','"+ M.getDestination()+"','" +M.getId_user()+"');";
        ste.executeUpdate(req);
        }
    }
    public void SupprimerMessage(int id) throws SQLException
    {
        String Requete = "DELETE FROM `message` WHERE id_message=" + id + "";
        ste.executeUpdate(Requete);
    }
   
    
        public List<Message> afficher() throws SQLException {
        String requete = "select * from message ";
        ResultSet resultset = ste.executeQuery(requete);

        List<Message> message = new ArrayList <Message>();
        resultset.beforeFirst();
        while (resultset.next()) {
           Message M = new Message();
            M.setId_msg(resultset.getInt(1));
            M.setContenu(resultset.getString(2));
            M.setDate_partage(resultset.getDate(3));
            M.setDocument(resultset.getString(4));
            M.setDestination(resultset.getInt(5));
            M.setId_user(resultset.getInt(6));

            message.add(M);
            System.out.println(M.toString());
        }
        
        return message;
    }
        public List<Message> afficherparcontenu(String contenu) throws SQLException {
            String requete = "select * from message WHERE contenu like'%\" + contenu + \"%' ";
        ResultSet resultset = ste.executeQuery(requete);

        List<Message> message = new ArrayList <Message>();
        resultset.beforeFirst();
        while (resultset.next()) {
           Message M = new Message();
            M.setId_msg(resultset.getInt(1));
            M.setContenu(resultset.getString(2));
            M.setDate_partage(resultset.getDate(3));
            M.setDocument(resultset.getString(4));
            M.setDestination(resultset.getInt(5));
            M.setId_user(resultset.getInt(6));

            message.add(M);
            System.out.println(M.toString());
        }
        
        return message;
        }
        
    }

