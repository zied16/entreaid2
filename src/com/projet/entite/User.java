/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entite;

import java.sql.Date;

/**
 *
 * @author Saif
 */
public class User {
    private int id_user;
    private String nom;
    private String prenom;
    private Date date_naissance;
    private String adresse;
    private String email;
    private int tel;
    private String sexe;
    private String pays_origine;
    private String login;
    private String password;
    private String image;
    private String role;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPays_origine() {
        return pays_origine;
    }

    public void setPays_origine(String pays_origine) {
        this.pays_origine = pays_origine;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User(int id_user, String nom, String prenom, Date date_naissance, String adresse, String email, int tel, String sexe, String pays_origine, String login, String password, String image, String role) {
        this.id_user = id_user;
        this.nom = nom;
        this.prenom = prenom;
        this.date_naissance = date_naissance;
        this.adresse = adresse;
        this.email = email;
        this.tel = tel;
        this.sexe = sexe;
        this.pays_origine = pays_origine;
        this.login = login;
        this.password = password;
        this.image = image;
        this.role = role;
    }
   
 }
