/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.service;

import com.projet.entite.Evenement;
import java.sql.*;
import com.esprit.utils.DataSource;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author zied
 */
public class ServiceEvenement {
    private Connection con=DataSource.getInstance().getConnection();
  
   private Statement ste;

    public ServiceEvenement() {
      try {
          ste=con.createStatement();
      } catch (SQLException ex) {
          System.out.println(ex);
      }
    }
     public void ajouterEvent(Evenement E)  throws SQLException
    {
     String requete="INSERT INTO `evenement`(`id_evenement`, `lieu`, `description`, `date_partage`, `date_evenement`, `image`, `duree`, `id_club`) "
             + "VALUES ('"+E.getId_evenement()+"','"+E.getLieu()+"','"+E.getDescription()+"',SYSDATE(),'"+E.getDate_event()+"',"
             + "'"+E.getImage()+"','"+E.getDuree()+"','"+E.getId_club()+"');";
             
             ResultSet resultat= ste.executeQuery("select * from users where role='Responsable' ") ;
             
                if(resultat.first())
                {
     
       ste.executeUpdate(requete);
       
                }
       
    }
    public void SupprimerEvent(long id) throws SQLException
    {
        String Requete = "DELETE FROM `evenement` WHERE id_evenement=" + id + "";
        ste.executeUpdate(Requete);
    }
    
    public List<Evenement> affichertout() {
         try {
             List<Evenement> event = new ArrayList<Evenement>();
            String reqete = "SELECT * FROM `evenement`";
           ste = con.createStatement();
            ResultSet res = ste.executeQuery(reqete);
            while (res.next()) {
                Evenement E = new Evenement();
                E.setId_evenement(res.getInt(1));
                E.setLieu(res.getString(2));
                E.setDescription(res.getString(3));
                E.setDate_partage(res.getDate(4));
                E.setDate_event(res.getDate(5));
                E.setImage(res.getString(6));
                E.setDuree(res.getLong(7));
                E.setId_club(res.getInt(8));
                
                
                
                event.add(E);
                System.out.println(E.toString());

            }
            return event ;
        }
        catch (SQLException ex) {
            
            return null; 
        }
    }
    public void updateEvent(Evenement E) 
    {
    
    try {
            String requete="UPDATE `evenement` SET lieu='" + E.getLieu()+ "' "
                    + ",description='" + E.getDescription()+ "',date_partage=SYSDATE() ,date_evenement='" + E.getDate_event()+  "'  "
                    + ",image='" + E.getImage()+ "',duree=" + E.getDuree()+ "  ,id_club='" + E.getId_club()+ "'   "
                   
                    + " where id_evenement =" + E.getId_evenement()+ ""; 
            
           
        
            
            ste.executeUpdate(requete);
            System.out.println("Mise à jour effectuée avec succès");
            
        } catch (SQLException ex) {
            System.out.println(ex);
            
        }
    }
    

public List<Evenement> afficherparclub(int id) {
         try {
             List<Evenement> event = new ArrayList<Evenement>();
            String reqete = "SELECT * FROM `evenement` where id_club=" +id+ "";
           ste = con.createStatement();
            ResultSet res = ste.executeQuery(reqete);
            while (res.next()) {
                Evenement E = new Evenement();
                E.setId_evenement(res.getInt(1));
                E.setLieu(res.getString(2));
                E.setDescription(res.getString(3));
                E.setDate_partage(res.getDate(4));
                E.setDate_event(res.getDate(5));
                E.setImage(res.getString(6));
                E.setDuree(res.getLong(7));
                E.setId_club(res.getInt(8));
                
                
                
                event.add(E);
                System.out.println(E.toString());

            }
            return event ;
        }
        catch (SQLException ex) {
            
            return null; 
        }
    }
             /* public static void main(String [] args)
{
    Date d=new Date(88,8,3);
     Evenement E=new Evenement(1,1,"vvv","yyy","vvv",d,99f);
        ServiceEvenement SE= new ServiceEvenement();
        
        
         try {
            //SE.affichertout();
            SE.SupprimerEvent(1);
        } catch (SQLException ex) {
             System.err.println(ex);
            
        }
            
            
}*/
}
  

