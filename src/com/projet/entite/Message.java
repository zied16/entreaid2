/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entite;

import java.sql.Date;

/**
 *
 * @author zied
 */
public class Message {
    int id_msg;
    String contenu;
    Date date_partage;
    String document;
    int destination;
    int id_user;

    public Message() {
    }

    public Message(int id_msg, String contenu, Date date_partage, String document, int destination, int id_user) {
        this.id_msg = id_msg;
        this.contenu = contenu;
        this.date_partage = date_partage;
        this.document = document;
        this.destination = destination;
        this.id_user = id_user;
    }

    public int getId_msg() {
        return id_msg;
    }

    public void setId_msg(int id_msg) {
        this.id_msg = id_msg;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDate_partage() {
        return date_partage;
    }

    public void setDate_partage(Date date_partage) {
        this.date_partage = date_partage;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "Message{" + "id_msg=" + id_msg + ", contenu=" + contenu + ", date_partage=" + date_partage + ", document=" + document + ", destination=" + destination + ", id_user=" + id_user + '}';
    }
    
    
}
