/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entite;

import java.sql.Date;

/**
 *
 * @author zied
 */
public class InscriptionGroupProjet {
    private int id_group_projet;
    private int id_user;
    private Date date_insription;

    public InscriptionGroupProjet() {
    }

    public InscriptionGroupProjet(int id_group_projet, int id_user, Date date_insription) {
        this.id_group_projet = id_group_projet;
        this.id_user = id_user;
        this.date_insription = date_insription;
    }

    public int getId_group_projet() {
        return id_group_projet;
    }

    public void setId_group_projet(int id_group_projet) {
        this.id_group_projet = id_group_projet;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public Date getDate_insription() {
        return date_insription;
    }

    public void setDate_insription(Date date_insription) {
        this.date_insription = date_insription;
    }

    @Override
    public String toString() {
        return "InscriptionGroup{" + "id_group_projet=" + id_group_projet + ", id_user=" + id_user + ", date_insription=" + date_insription + '}';
    }
    
    
    
}
