/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.utils;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zied
 */
public class DataSource {

    private static DataSource data;
    private static Connection con;

    private DataSource() {

        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetesprit2", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public Connection getConnection()
        {
            return con;
    }
    
     public static DataSource getInstance() {
        if (data == null) {
             data = new DataSource();
        }

        return data;
    }
}
