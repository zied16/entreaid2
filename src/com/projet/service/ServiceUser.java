/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.service;
import com.projet.entite.User;
import java.sql.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.esprit.utils.DataSource;
/**
 *
 * @author Saif
 */
public class ServiceUser {
    
     private Connection con = DataSource.getInstance().getConnection();
    private Statement ste;
    private PreparedStatement mystmt = null;
    FileInputStream input = null;

    public ServiceUser() {
        try {
            ste = con.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    public void AjouterUser(User u)
    {
    String Req="INSERT INTO `users` (`id_users`, `nom`, `prenom`, `date_naissance`, `last_login`, `adresse`, `email`, `num_telephone`, `sexe`, `pays_origine`, `login`, `password`, `image`, `role`) VALUES"
                          + " (NULL, '"+u.getNom()+"', '"+u.getPrenom()+"', '"+u.getDate_naissance()+"',SYSDATE(),'"+u.getAdresse()+"', '"+u.getEmail()+"', '"+u.getTel()+"', '"+u.getSexe()+"', '"+u.getPays_origine()+"', '"+u.getLogin()+"', '"+u.getPassword()+"',?, '"+u.getRole()+"')";
         try {
        mystmt = con.prepareStatement(Req);
        File MyPicture = new File(u.getImage());
        try {
            input = new FileInputStream(MyPicture);
        } catch (FileNotFoundException ex) {
            System.out.println("Fichier introuvable");
        }
        mystmt.setBinaryStream(1, input);
        mystmt.executeUpdate();
         } catch (SQLException ex) {
             System.out.println(ex);
         }
    }

public void RechercherUser(int rech) throws SQLException {
        ResultSet result = ste.executeQuery("select * from users where id_users=" + rech + "");
        while (result.next()) {
            int id = result.getInt(1);
            String nom = result.getString(2);
            String prenom=result.getString(3);
            Date date_naissance=result.getDate(4);
            String adresse=result.getString(6);
            String email=result.getString(7);
            int tel=result.getInt(8);
            String sexe=result.getString(9);
            String pays_origine=result.getString(10);
            String login=result.getString(11);
            String password=result.getString(12);
            String role=result.getString(14);

            
            System.out.println(" id : " + id + " nom : " + nom+" prenom : "+prenom+ 
                    " date naissance: "+date_naissance+" adresse: "+adresse+" email: "+email+"\nnum tel: "+tel+
                    " sexe: "+sexe+" pays d'origine: "+pays_origine+"\nlogin: "+login+" mot de passe: "+password+
                    " role: "+role);
//kamalha
        }
    }

public void SupprimerUser(int rech) throws SQLException {
        String Req = "DELETE FROM `users` WHERE id_users=" + rech + "";
        ste.executeUpdate(Req);
    }
public void UpdateUser(User u) throws SQLException
{
String Req="UPDATE `users` SET `nom`='"+u.getNom()+"',`prenom`='"+u.getPrenom()+"',`date_naissance`='"+u.getDate_naissance()+"',`adresse`='"+u.getAdresse()+"',"
        + "`email`='"+u.getEmail()+"',`num_telephone`="+u.getTel()+",`sexe`='"+u.getSexe()+"',`pays_origine`='"+u.getPays_origine()+"',`login`='"+u.getLogin()+"',`password`='"+u.getPassword()+"',`image`='"+u.getImage()+"',`role`='"+u.getRole()+"' WHERE id_users="+u.getId_user()+"";
ste.executeUpdate(Req);
}
}

    

   