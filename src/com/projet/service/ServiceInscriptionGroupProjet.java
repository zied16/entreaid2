/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.service;

import com.esprit.utils.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.projet.entite.InscriptionGroupProjet;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zied
 */
public class ServiceInscriptionGroupProjet {
    private Connection con=DataSource.getInstance().getConnection();
  
   private Statement ste;

    public ServiceInscriptionGroupProjet () {
      try {
          ste=con.createStatement();
      } catch (SQLException ex) {
          System.out.println(ex);
      }
    }
    public void AjouterInscriptionGP(InscriptionGroupProjet I) throws SQLException {
     String requete="select * from users where role='Etudiant' and id_users=" + I.getId_user() + "";
     
    ResultSet res= ste.executeQuery(requete);
        if (res.first()) {
          
            String requete1="select * from groupe_projet where id_groupe_projet=" + I.getId_group_projet() + "";
        ResultSet res1= ste.executeQuery(requete1);
            if (res1.first()) {

                String requetef = "INSERT INTO `inscription_projet`(`id_groupe_projet`, `id_users`,`date_inscription`) "
                        +"VALUES (" + I.getId_group_projet() + "," + I.getId_user() + ",SYSDATE())";
                ste.executeUpdate(requetef);
            }
        }
    }
    public void supprimer(int id) throws SQLException
    {
        String Requete = "DELETE FROM `inscription_projet` WHERE id_groupe_projet=" + id +"" ;
        ste.executeUpdate(Requete);
    
    }
    
    public List<InscriptionGroupProjet> afficher() throws SQLException {
        String requete = "select * from inscription_projet";
        ResultSet resultset = ste.executeQuery(requete);

        List<InscriptionGroupProjet> group = new ArrayList <InscriptionGroupProjet>();
        resultset.beforeFirst();
        while (resultset.next()) {
            InscriptionGroupProjet p = new InscriptionGroupProjet();
            p.setId_group_projet(resultset.getInt(1));
            p.setId_user(resultset.getInt(2));
            p.setDate_insription(resultset.getDate(3));

            group.add(p);
            System.out.println(p.toString());
        }
        
        return group;
    }
    public List<InscriptionGroupProjet> rechercher(int id ) throws SQLException {
        String requete = "select * from inscription_projet where id_groupe_projet ="+id+"";
        ResultSet resultset = ste.executeQuery(requete);

        List<InscriptionGroupProjet> group = new ArrayList <InscriptionGroupProjet>();
        resultset.beforeFirst();
        while (resultset.next()) {
            InscriptionGroupProjet p = new InscriptionGroupProjet();
            p.setId_group_projet(resultset.getInt(1));
            p.setId_user(resultset.getInt(2));
            p.setDate_insription(resultset.getDate(3));

            group.add(p);
            System.out.println(p.toString());
        }
        
        return group;
    }
        /*public void updateInsGrpr(InscriptionGroupProjet I) throws SQLException
    {
    
    
            String requete="UPDATE `inscription_projet` SET id_groupe_projet='" + I.getId_group_projet()+ "' "
                    + ",id_users='" + I.getId_user()+ "',date_inscription=SYSDATE() "; 
            
           
            ste.executeUpdate(requete);
            System.out.println("Mise à jour effectuée avec succès");
            
        
    }*/
    
    
    public static void main(String [] args)
    {
        
           Date d=new Date(88,8,3);
            ServiceInscriptionGroupProjet SIG =new ServiceInscriptionGroupProjet();
            
            InscriptionGroupProjet A =new InscriptionGroupProjet(2, 100, d);
           try {
               //SIG.AjouterInscriptionGP(A);
               //SIG.afficher();
              // SIG.rechercher(2);
              //A.setId_group_projet(6);
              SIG.supprimer(2);
              
        } catch (SQLException ex) {
             System.err.println(ex);
            
        }
    }
    
    
}
